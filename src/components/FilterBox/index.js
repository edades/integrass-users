import React from 'react'
import './styles.css'

const FitlerBox = ({ setNameTerm, nameTerm }) => {
  return (
    <div className="filterBox">
      <label>Search by name:&nbsp;&nbsp;
        <input type="text" value={nameTerm} onChange={(e) => setNameTerm(e.target.value)} />
      </label>
    </div>
  )
}

export default FitlerBox
