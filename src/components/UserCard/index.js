import React from 'react'
import './styles.css'

const UserCard = ({ user, deleteUser }) => {
  return (
    <div className="userCard">
      <span className="userCard--delete" onClick={() => deleteUser(user.id)}>X</span>
      <div className="userCard--username">
        {user.username}
      </div>
      <div className="userCard--data">
        <p style={{ fontSize: 18 }}>{user.name}</p>
        <p>
          <span>Phone:</span>
          <a href={`tel:${user.phone}`}>{user.phone}</a>
        </p>
        <p>
          <span>Email:</span>
          <a href={`mailto:${user.email}`}>{user.email}</a>
        </p>
      </div>
    </div>
  )
}

export default UserCard
