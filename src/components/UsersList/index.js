import React, { useEffect, useState } from 'react'
import './styles.css'
import UserCard from '../UserCard'

const UsersList = ({ nameTerm }) => {
  const [users, setUsers] = useState([])

  const deleteUser = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
      method: 'DELETE',
    })
    .then(response => {
      if (response.status === 200) {
        setUsers(users.filter(user => user.id !== id))
      }
    })
  }

  const getUsers = () => {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(jsonUsers => setUsers(jsonUsers))
  }

  useEffect(() => {
    getUsers()
  }, [])

  const getUserList = () => {
    const filteredUsers = users.filter(user =>
      user.name.toLowerCase().includes(nameTerm.toLowerCase()))
    if (nameTerm && !filteredUsers.length) {
      return <h1>No results for <i>"{nameTerm}"</i></h1>
    }
    return filteredUsers.map(user =>
      <UserCard user={user} deleteUser={deleteUser} key={user.id} />)
  }

  if (!users.length) return <h1>Loading data...</h1>
  return (
    <div className="usersList">
      {getUserList()}
    </div>
  )
}

export default UsersList
