import React, { useState } from 'react'
import './styles.css'
import Header from '../../components/Header'
import UsersList from '../../components/UsersList'
import FilterBox from '../../components/FilterBox'

const HomeContainer = () => {
  const [nameTerm, setNameTerm] = useState('')
  return (
    <div className="homeContainer">
      <Header />
      <FilterBox nameTerm={nameTerm} setNameTerm={setNameTerm} />
      <UsersList nameTerm={nameTerm} />
    </div>
  )
}

export default HomeContainer
